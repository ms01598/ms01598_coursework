/**
 * 
 */
package ms01598_coursework;

import java.sql.SQLException;

/**
 @author ms01598
 * This is the class with the main method to enable the checking and representing of the results of the methods.
 */
public class Requirement3 {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		ProductsDataAccess obj3 = new ProductsDataAccess();  //object is created to use the methods
		obj3.getSameProductLines(); //the products of the same product line are stored in the arraylist
		obj3.showProfit(); //the profit is shown for each product line in a descending order
		obj3.closeConnection(); //the connection is terminated

	}

}
