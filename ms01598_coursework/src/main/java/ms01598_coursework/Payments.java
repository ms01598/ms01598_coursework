/**
 * 
 */
package ms01598_coursework;

/**
 * @author ms01598
 *  A class to enable the creation of objects of a specific table, in this case the table Payments.
 *
 */
public class Payments {
		// local field variables
		private int customerNumber;
		private String checkNumber;
		private String paymentDate;
		private double amount;

		/**This is the parameterized constructor of the class that creates the object.
		 * @param the customer number
		 * @param the check number
		 * @param the payment date
		 * @param the amount paid*/
		public Payments(int customerNumber, String checkNumber, String paymentDate, double amount) {
			super();
			this.customerNumber = customerNumber;
			this.checkNumber = checkNumber;
			this.paymentDate = paymentDate;
			this.amount = amount;
		}
		
		/**Below are the getters generated to make the values of the object available.*/

		public int getCustomerNumber() {
			return this.customerNumber;
		}

		public String getCheckNumber() {
			return this.checkNumber;
		}

		public String getPaymentDate() {
			return this.paymentDate;
		}

		public double getAmount() {
			return this.amount;
		}

	

}
