/**
 * 
 */
package ms01598_coursework;

import java.sql.SQLException;

/**
 * @author ms01598
 * This is the class with the main method to enable the checking and representing of the results of the methods.
 *
 */
public class Requirement1 {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		PaymentsDataAccess obj = new PaymentsDataAccess(); //object is created to use the methods
		obj.getPayments(); //the payments are stored in the arraylist
		obj.showPayments(); //the payments are displayed on the console
		obj.closeConnection(); //the connection is terminated
		
	}

}
