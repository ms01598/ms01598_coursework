/**
 * 
 */
package ms01598_coursework;

/**
 * A class to enable the creation of objects of a specific table, in this case the table Order Details.
 * @author ms01598
 */
public class OrderDetails {
	//local field variables
	private int orderNumber;
	private String productCode;
	private int quantityOrdered;
	private double priceEach;
	private int orderLineNumber;
	
	/**This is the parameterised constructor of the class that creates the object.
	 * @param the order number
	 * @param the product code
	 * @param the quantity ordered
	 * @param the price for each product
	 * @param the order line number*/
	public OrderDetails (int orderNumber, String productCode, int quantityOrdered, double priceEach, int orderLineNumber) {
		this.orderNumber = orderNumber;
		this.productCode = productCode;
		this.quantityOrdered = quantityOrdered;
		this.priceEach = priceEach;
		this.orderLineNumber = orderLineNumber;
	}
	
	/**Below are the getters generated to make the values of the object available.*/
	
	public int getOrderNumber() {
		return orderNumber;
	}

	public String getProductCode() {
		return productCode;
	}

	public int getQuantityOrdered() {
		return quantityOrdered;
	}

	public double getPriceEach() {
		return priceEach;
	}

	public int getOrderLineNumber() {
		return orderLineNumber;
	}

	
}
