/**
 * 
 */
package ms01598_coursework;

/**
 * @author ms01598
 * To create a connection with the database.
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;

public class GetConnected {
	protected Connection connection = null; //Initializing the variable to store the connection

	/**Creating a method to call when a connection with the database need to be achieved.
	 * @return the connection
	 * @exception RuntimeException*/
	public Connection get_connection(){
		try {
		   	DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ()); 
			connection = DriverManager.getConnection( "jdbc:mysql://localhost:3306/classicmodels", "root", "1234"); //the link to the db and the username and password for the server
		}
		catch(Exception exc) {
			System.out.println("Connection failed!");
			throw new RuntimeException(exc); //for the case that the connection fails
		}
		return connection;
	}

}
