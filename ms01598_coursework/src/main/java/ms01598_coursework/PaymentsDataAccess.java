/**
 * 
 */
package ms01598_coursework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


import java.sql.*;
/**
 * @author ms01598
 * This class is the Data Access Object for the Payments table and it is also where the methods used are constructed.
 */
public class PaymentsDataAccess {
	/**First a connection object needs to be instantiated for the connection to be used by the main method.*/
	GetConnected con = new GetConnected();
	Connection connection = con.get_connection();
	
	Statement stmnt = null;
	ResultSet results = null;
	
	/**This is the method where the data acquired from the database are inserted into an ArrayList of an object of type Payments.
	 * In that way it is easier to handle the data with different methods.*/
	public List<Payments> getPayments() throws SQLException {
		ArrayList<Payments> totalPayments = new ArrayList<Payments>();
		try {
				Statement stmnt = connection.createStatement();//an SQL statement object is executed to generate the result set object
				ResultSet results = stmnt.executeQuery("SELECT * FROM Payments WHERE amount > 100000");// the query is sent to the database and returned to a table of data representing a database result set
				
				/**Now inside the wile loop the resultset is being iterated and the data is stored to each separate element of the table to a variable
				 * which are then added to the ArrayList as created Payments object.*/
			while (results.next()) {
				int customerNumber = results.getInt("customerNumber");
				String checkNumber = results.getString("checkNumber");
				String paymentDate = results.getString("paymentDate");
				double amount = results.getDouble("amount");
				totalPayments.add(new Payments(customerNumber, checkNumber, paymentDate, amount));
			}

		} catch (SQLException e) {
			System.out.println("Retrieving records failed");
			throw new RuntimeException(e);
		}
		return totalPayments;
}
	/**This method is iterating through the created arraylist and displaying the required results.*/
	public void showPayments() throws SQLException {
		List<Payments> totalPayments = getPayments();
		Iterator<Payments> i= totalPayments.iterator();

		Payments temp; //temporary Payments object
		while (i.hasNext()) {
			temp = i.next();
			System.out.println("Customer No: " + temp.getCustomerNumber());
			System.out.println("Check No:" + temp.getCheckNumber());
			System.out.println("Date: " + temp.getPaymentDate());
			System.out.println("Amount: " + temp.getAmount() + "\n");
		}
	}
	
	/**The connection needs to be terminated after the data is acquired. */
	public void closeConnection() {

		try {
			if (this.stmnt != null) {
				this.stmnt.close();
			}
			if (this.connection != null) {
				this.connection.close();
			}
			System.out.println("Closed the connection to the database");
		} catch (Exception e) {
			System.out
					.print("Failed to close the connection to the database!");
			throw new RuntimeException(e);
		}
	}
	
}