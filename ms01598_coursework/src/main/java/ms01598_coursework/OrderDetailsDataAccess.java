/**
 * 
 */
package ms01598_coursework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ms01598
 *	This class is the Data Access Object for the Order Details table and it is also where the methods used are constructed.
 */
public class OrderDetailsDataAccess {
	/**First a connection object needs to be instantiated for the connection to be used by the main method.*/
	GetConnected con = new GetConnected();
	Connection connection = con.get_connection();
	
	Statement stmnt = null;
	ResultSet results = null;
	
	/**This is the method where the data acquired from the database are inserted into an ArrayList of an object of type OrderDetails.
	 * In that way it is easier to handle the data with different methods.*/
	public List<OrderDetails> getOrderDetails() throws SQLException {
		ArrayList<OrderDetails> totalOrderDetails = new ArrayList<OrderDetails>();
		try {
				Statement stmnt = connection.createStatement(); //an SQL statement object is executed to generate the result set object
				ResultSet results = stmnt.executeQuery("SELECT * FROM OrderDetails"); // the query is sent to the database and returned to a table of data representing a database result set
				/**Now inside the wile loop the resultset is being iterated and the data is stored to each separate element of the table to a variable
				 * which are then added to the ArrayList as created OrderDetails object.*/
			while (results.next()) {
				int orderNumber = results.getInt("orderNumber");
				String productCode = results.getString("productCode");
				int quantityOrdered = results.getInt("quantityOrdered");
				double priceEach = results.getDouble("priceEach");
				int orderLineNumber = results.getInt("orderLineNumber");
				totalOrderDetails.add(new OrderDetails(orderNumber, productCode, quantityOrdered, priceEach, orderLineNumber));
			}

		} catch (SQLException e) {
			System.out.println("Retrieving records failed");
			throw new RuntimeException(e); 
		}
		return totalOrderDetails;
}
	/**The connection needs to be terminated after the data is acquired. */
	public void closeConnection() {

		try {
			if (this.stmnt != null) {
				this.stmnt.close();
			}
			if (this.connection != null) {
				this.connection.close();
			}
			System.out.println("Closed the connection to the database");
		} catch (Exception e) {
			System.out.print("Failed to close the connection to the database!");
			throw new RuntimeException(e);
		}
	}

}
