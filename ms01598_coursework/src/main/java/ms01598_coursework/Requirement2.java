/**
 * 
 */
package ms01598_coursework;

import java.sql.SQLException;

/**
  * @author ms01598
 * This is the class with the main method to enable the checking and representing of the results of the methods.
 *
 */
public class Requirement2 {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		ProductsDataAccess obj2 = new ProductsDataAccess(); //object is created to use the methods
		obj2.getProducts(); //the products are stored in the arraylist
		obj2.showProducts();  //the products are displayed on the console
		obj2.closeConnection(); //the connection is terminated
		
	}

}
