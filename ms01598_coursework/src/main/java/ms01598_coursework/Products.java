/**
 * 
 */
package ms01598_coursework;

/**
 * @author ms01598
 *  A class to enable the creation of objects of a specific table, in this case the table Products.
 *
 */
public class Products {
	// local field variables
	private String productCode;
	private String productName;
	private String productScale;
	private String productVendor;
	private String productDescription;
	private int quantityInStock;
	private double buyPrice;
	private double MSRP;
	private String productLine;
	
	/**This is the parameterized constructor of the class that creates the object.
	 * @param the product code
	 * @param the name of the product
	 * @param the product scale
	 * @param the product's vendor
	 * @param the description of the product
	 * @param the quantity of the stock of the product
	 * @param the price that the product is bought
	 * @param the Manufacturer Suggested Retail Price
	 * @param the product Line*/
	public Products (String productCode, String productName, String productScale, String productVendor,
			String productDescription,int quantityInStock, double buyPrice, double MSRP, String productLine) {
		super();
		this.productCode = productCode;
		this.productName = productName;
		this.productScale = productScale;
		this.productVendor = productVendor;
		this.productDescription = productDescription;
		this.quantityInStock = quantityInStock;
		this.buyPrice = buyPrice;
		this.MSRP = MSRP;
		this.productLine = productLine;
	}

	/**Below are the getters generated to make the values of the object available.*/
	
	public String getProductCode() {
		return productCode;
	}

	public String getProductName() {
		return productName;
	}

	public String getProductScale() {
		return productScale;
	}

	public String getProductVendor() {
		return productVendor;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public int getQuantityInStock() {
		return quantityInStock;
	}

	public double getBuyPrice() {
		return buyPrice;
	}

	public double getMSRP() {
		return MSRP;
	}

	public String getProductLine() {
		return productLine;
	}
	
	

}
