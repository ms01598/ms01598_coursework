/**
 * 
 */
package ms01598_coursework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author ms01598
 * This class is the Data Access Object for the Products table and it is also where the methods used are constructed.
 *
 */
public class ProductsDataAccess {
	/**First a connection object needs to be instantiated for the connection to be used by the main method.*/
	GetConnected con = new GetConnected();
	Connection connection = con.get_connection();
	
	Statement stmnt = null;
	ResultSet results = null;
	
	/**This is the method where the data acquired from the database are inserted into an ArrayList of an object of type Products.
	 * In that way it is easier to handle the data with different methods.*/
	public List<Products> getProducts() throws SQLException {
		ArrayList<Products> totalProducts = new ArrayList<Products>();
		try {
				Statement stmnt = connection.createStatement();//an SQL statement object is executed to generate the result set object
				ResultSet results = stmnt.executeQuery("SELECT * FROM Products WHERE NOT EXISTS (SELECT * FROM OrderDetails WHERE Products.productCode = OrderDetails.productCode)");
	
				/**Now inside the wile loop the resultset is being iterated and the data is stored to each separate element of the table to a variable
				 * which are then added to the ArrayList as created Payments object.*/
			while (results.next()) {
				String productCode = results.getString("productCode");
				String productName = results.getString("productName");
				String productScale = results.getString("productScale");
				String productVendor = results.getString("productVendor");
				String productDescription = results.getString("productDescription");
				int quantityInStock = results.getInt("quantityInStock");
				double buyPrice = results.getDouble("buyPrice");
				double MSRP = results.getDouble("MSRP");
				String productLine = results.getString("productLine");
				totalProducts.add(new Products(productCode, productName, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP, productLine));
			}

		} catch (SQLException e) {
			System.out.println("Retrieving records failed");
			throw new RuntimeException(e);
		}
		return totalProducts;
}
	/**This method is iterating through the created arraylist and displaying the required results.*/
	public void showProducts() throws SQLException {
		List<Products> totalProducts = getProducts();
		Iterator<Products> i= totalProducts.iterator();

		Products temp; //temporary Products object
		while (i.hasNext()) {
			temp = i.next();
			System.out.println("Product Code: " + temp.getProductCode());
			System.out.println("Name: " + temp.getProductName());
			System.out.println("Scale: " + temp.getProductScale());
			System.out.println("Vendor: " + temp.getProductVendor());
			System.out.println("Description: " + temp.getProductDescription());
			System.out.println("In Stock: " + temp.getQuantityInStock());
			System.out.println("Price Bought: " + temp.getBuyPrice());
			System.out.println("Manufacturer Suggested Retail Price: " + temp.getMSRP());
			System.out.println("Product Line: " + temp.getProductLine() + "\n");
	}
}
	/**The connection needs to be terminated after the data is acquired. */
	public void closeConnection() {

		try {
			if (this.stmnt != null) {
				this.stmnt.close();
			}
			if (this.connection != null) {
				this.connection.close();
			}
			System.out.println("Closed the connection to the database");
		} catch (Exception e) {
			System.out
					.print("Failed to close the connection to the database!");
			throw new RuntimeException(e);
		}
	}

	/**For the third requirement the total profit of each product line needs to be computed and sorted by profit descending.
	 * So the profit is the difference between MSRP and buyPrice. They are grouped by each product line and appear from the largest to the smallest profit.
	 */
	public List<Products> getSameProductLines() throws SQLException {
		ArrayList<Products> sameProductLines = new ArrayList<Products>();
		try {
				Statement stmnt = connection.createStatement();
				ResultSet results = stmnt.executeQuery("SELECT Products.productLine, SUM(Products.MSRP-Products.buyPrice) AS profit FROM Products GROUP BY productLine ORDER BY profit DESC;");

			while (results.next()) {
				double profit = results.getDouble("profit");
				String productLine = results.getString("productLine");
				sameProductLines.add(new Products(null, null, null, null, null, 0, 0, profit, productLine));
			}

		} catch (SQLException e) {
			System.out.println("Retrieving records failed");
			throw new RuntimeException(e);
		}
		return sameProductLines;

	}
	
	/**This method is used to show the profits by each product line. */
	public void showProfit() throws SQLException {
		List<Products> totalProfits = getSameProductLines();
		Iterator<Products> i= totalProfits.iterator();
		
		Products temp;//temporary Products variable
		while (i.hasNext()) {
			temp = i.next();
			System.out.println("Product Line: " + temp.getProductLine() + " | Profit: " + temp.getMSRP());
		}
	}
}