package ms01598_coursework;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class Requirement2Test {

	@Test
	public void test() {
		 GetConnected con = new GetConnected();
	       try ( Connection connection = con.get_connection())
	        {
	            try(Statement st=connection.createStatement())
	            {
	                connection.setAutoCommit(false);

	                
	                //Setting input parameters taken from the db as examples:
	                String productCode = "S18_3233";
	            	String productName = "1985 Toyota Supra";
	            	String productScale = "1:18";
	            	String productVendor = "Highway 66 Mini Classics";
	            	String productDescription = "This model features soft rubber tires, working steering, rubber mud guards, authentic Ford logos, detailed undercarriage, opening doors and hood, removable split rear gate, full size spare mounted in bed, detailed interior with opening glove box";
	            	int quantityInStock = 7733;
	            	double buyPrice = 57.01;
	            	double MSRP = 107.57;
	            	String productLine = "Classic Cars";

	                Products product= new Products(productCode, productName, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP, productLine);

	                //Check that contains the expected values:
	                assertEquals(productCode, product.getProductCode());
	                assertEquals(productName, product.getProductName());
	                assertEquals(productScale, product.getProductScale());
	                assertEquals(productVendor, product.getProductVendor());
	                assertEquals(productDescription, product.getProductDescription());
	                assertEquals(quantityInStock, product.getQuantityInStock());
	                assertEquals(buyPrice, product.getBuyPrice(), 0.01);
	                assertEquals(MSRP, product.getMSRP(), 0.01);
	                assertEquals(productLine, product.getProductLine());
	        

	                //Database Checks: check the Products table contains one row with the expected values:
	                try(ResultSet rs=st.executeQuery("SELECT * FROM Products WHERE NOT EXISTS (SELECT * FROM OrderDetails WHERE Products.productCode = OrderDetails.productCode)"))
	                {
	                    assertTrue(rs.next());
	                    assertEquals(productCode, rs.getString("productCode"));
		                assertEquals(productName, rs.getString("productName"));
		                assertEquals(productScale, rs.getString("productScale"));
		                assertEquals(productVendor, rs.getString("productVendor"));
		                assertEquals(productDescription, rs.getString("productDescription"));
		                assertEquals(quantityInStock, rs.getString("quantityInStock"));
		                assertEquals(buyPrice, rs.getString("buyPrice"));
		                assertEquals(MSRP, rs.getString("MSRP"));
		                assertEquals(productLine, rs.getString("productLine"));
	                    assertFalse(rs.next());
	                } catch (AssertionError error) {
	                		                	
	                }
	                
	            }
	            finally
	            {
	                 //Undo the testing operations:
	                 connection.rollback();
	            }
	        }
	        catch (SQLException e)
	        {
	            fail(e.toString());
	        }
	    }
	

}
