package ms01598_coursework;

import static org.junit.Assert.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.*;
import org.junit.Test;

public class Requirement1Test {
	
	/**
	 * @author ms01598
	 * For the testing the values acquired , sorted and manipulated with an ArrayList through java code need to be tested against the actual data
	 * on the database for the table used. 
	 * */

	@Test
	public void test() {
				
		        GetConnected con = new GetConnected();
		       try ( Connection connection = con.get_connection())
		        {
		            try(Statement st=connection.createStatement())
		            {
		                connection.setAutoCommit(false);

		                // Initial cleanup:
		                st.executeUpdate("DELETE FROM payments");
		                
		                // Setting input parameters taken from the db as example.
		                int customerNumber=124;
		                String checkNumber="AE215433";
		                String paymentDate="2005-03-05";
		                double amount = 101244.59;

		                Payments payment= new Payments(customerNumber, checkNumber, paymentDate, amount);

		                //Check that contains the expected values:
		                assertEquals(customerNumber, payment.getCustomerNumber());
		                assertEquals(checkNumber, payment.getCheckNumber());
		                assertEquals(paymentDate, payment.getPaymentDate());
		                assertEquals(amount, payment.getAmount(), 0.01);
		        

		                // Database Checks: check the Payments table contains one row with the expected values:
		                try(ResultSet rs=st.executeQuery("SELECT * FROM Payments WHERE amount > 100000"))
		                {
		                    assertTrue(rs.next());
		                    assertEquals(customerNumber, rs.getString("customerNumber"));
			                assertEquals(checkNumber, rs.getString("checkNumber"));
			                assertEquals(paymentDate, rs.getString("paymentDate"));
			                assertEquals(amount, rs.getString("amount"));
		                    assertFalse(rs.next());
		                } catch (AssertionError error) {
		                		                	
		                }
		                	                
		        }
		            finally
		            {
		                 //Undo the testing operations:
		                 connection.rollback();
		            }
		        }
		        catch (SQLException e)
		        {
		            fail(e.toString());
		        }
		    }
		
	}




