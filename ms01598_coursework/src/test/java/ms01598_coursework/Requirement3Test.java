/**
 * 
 */
package ms01598_coursework;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;


public class Requirement3Test {

	@Test
	public void test() {
		 GetConnected con = new GetConnected();
	       try ( Connection connection = con.get_connection())
	        {
	            try(Statement st=connection.createStatement())
	            {
	                connection.setAutoCommit(false);

	           
	                
	                // Setting input parameters taken from the db as example.
	                String productCode = "S10_1949";
	            	String productName = "1952 Alpine Renault 1300";
	            	String productScale = "1:10";
	            	String productVendor = "Classic Metal Creations";
	            	String productDescription = "Turnable front wheels; steering function; detailed interior; detailed engine; opening hood; opening trunk; opening doors; and detailed chassis.";
	            	int quantityInStock = 7305;
	            	double buyPrice = 98.58;
	            	double MSRP = 214.30;
	            	String productLine = "Classic Cars";

	                Products product= new Products(productCode, productName, productScale, productVendor, productDescription, quantityInStock, buyPrice, MSRP, productLine);

	                //Check that contains the expected values:
	                assertEquals(productCode, product.getProductCode());
	                assertEquals(productName, product.getProductName());
	                assertEquals(productScale, product.getProductScale());
	                assertEquals(productVendor, product.getProductVendor());
	                assertEquals(productDescription, product.getProductDescription());
	                assertEquals(quantityInStock, product.getQuantityInStock());
	                assertEquals(buyPrice, product.getBuyPrice(), 0.01);
	                assertEquals(MSRP, product.getMSRP(), 0.01);
	                assertEquals(productLine, product.getProductLine());
	        

	                //Database Checks: check the Products table contains one row with the expected values:
	                try(ResultSet rs=st.executeQuery("SELECT * FROM Products WHERE EXISTS (SELECT * FROM Products WHERE Products.productCode = Products.productCode) ORDER BY productLine ASC"))
	                {
	                    assertTrue(rs.next());	                   
	                    assertEquals(productCode, rs.getString("productCode"));
		                assertEquals(productName, rs.getString("productName"));
		                assertEquals(productScale, rs.getString("productScale"));
		                assertEquals(productVendor, rs.getString("productVendor"));
		                assertEquals(productDescription, rs.getString("productDescription"));
		                assertEquals(quantityInStock, rs.getString("quantityInStock"));
		                assertEquals(buyPrice, rs.getString("buyPrice"));
		                assertEquals(MSRP, rs.getString("MSRP"));
		                assertEquals(productLine, rs.getString("productLine"));
	                    assertFalse(rs.next());
	                } catch (AssertionError error) {
	                		                	
	                }
	     
	            }
	            finally
	            {
	                 //Undo the testing operations:
	                 connection.rollback();
	            }
	        }
	        catch (SQLException e)
	        {
	            fail(e.toString());
	        }
	    }
	


}
